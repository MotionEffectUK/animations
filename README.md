# README #

All Motion Effect's Motion Capture Project Work

### What is this repository for? ###

* Our Sample Asset Packages - Soldier, Boxing, Civilian
* FBX / Motion Builder Files of Animations.
* Raw Mocap Data (BVH Files)
* Documentation
* ScreenShots

### The Team ###

* Phil Payne
* Brian Powell
